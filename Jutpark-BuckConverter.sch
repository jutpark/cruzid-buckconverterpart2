<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.6.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="no" active="no"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="yes" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="4" fill="1" visible="yes" active="yes"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="no" active="yes"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="no" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="Lab5">
<packages>
<package name="T05A_TEX">
<pad name="1" x="0" y="0" drill="1.0922" diameter="1.6002"/>
<pad name="2" x="1.7018" y="0" drill="1.0922" diameter="1.6002"/>
<pad name="3" x="3.4036" y="0" drill="1.0922" diameter="1.6002"/>
<pad name="4" x="5.1054" y="0" drill="1.0922" diameter="1.6002"/>
<pad name="5" x="6.8072" y="0" drill="1.0922" diameter="1.6002"/>
<wire x1="-1.9812" y1="-1.9812" x2="8.7884" y2="-1.9812" width="0.1524" layer="21"/>
<wire x1="8.7884" y1="-1.9812" x2="8.7884" y2="2.9972" width="0.1524" layer="21"/>
<wire x1="8.7884" y1="2.9972" x2="-1.9812" y2="2.9972" width="0.1524" layer="21"/>
<wire x1="-1.9812" y1="2.9972" x2="-1.9812" y2="-1.9812" width="0.1524" layer="21"/>
<wire x1="-3.556" y1="0" x2="-3.9624" y2="0" width="0.1524" layer="21" curve="-180"/>
<wire x1="-3.9624" y1="0" x2="-3.556" y2="0" width="0.1524" layer="21" curve="-180"/>
<wire x1="-0.254" y1="0" x2="0.254" y2="0" width="0.1524" layer="51"/>
<wire x1="0" y1="-0.254" x2="0" y2="0.254" width="0.1524" layer="51"/>
<wire x1="-1.8796" y1="-1.8288" x2="8.6868" y2="-1.8288" width="0.1524" layer="51"/>
<wire x1="8.6868" y1="-1.8288" x2="8.6868" y2="2.8448" width="0.1524" layer="51"/>
<wire x1="8.6868" y1="2.8448" x2="-1.8796" y2="2.8448" width="0.1524" layer="51"/>
<wire x1="-1.8796" y1="2.8448" x2="-1.8796" y2="-1.8288" width="0.1524" layer="51"/>
<wire x1="-1.1684" y1="0" x2="-1.5748" y2="0" width="0" layer="51" curve="-180"/>
<wire x1="-1.5748" y1="0" x2="-1.1684" y2="0" width="0" layer="51" curve="-180"/>
<text x="0.127" y="-0.127" size="1.27" layer="25" ratio="6" rot="SR0">&gt;Name</text>
<text x="1.6764" y="-0.127" size="1.27" layer="27" ratio="6" rot="SR0">&gt;Value</text>
</package>
</packages>
<symbols>
<symbol name="LM2576T-5_0_1">
<pin name="VIN" x="2.54" y="0" length="middle" direction="pwr"/>
<pin name="OUT" x="2.54" y="-2.54" length="middle" direction="pwr"/>
<pin name="GND" x="63.5" y="-5.08" length="middle" direction="pwr" rot="R180"/>
<pin name="FB" x="63.5" y="-2.54" length="middle" direction="in" rot="R180"/>
<pin name="~ON~/OFF" x="63.5" y="0" length="middle" direction="in" rot="R180"/>
<wire x1="7.62" y1="5.08" x2="7.62" y2="-10.16" width="0.1524" layer="94"/>
<wire x1="7.62" y1="-10.16" x2="58.42" y2="-10.16" width="0.1524" layer="94"/>
<wire x1="58.42" y1="-10.16" x2="58.42" y2="5.08" width="0.1524" layer="94"/>
<wire x1="58.42" y1="5.08" x2="7.62" y2="5.08" width="0.1524" layer="94"/>
<text x="28.2956" y="9.1186" size="2.0828" layer="95" ratio="6" rot="SR0">&gt;Name</text>
<text x="27.6606" y="6.5786" size="2.0828" layer="96" ratio="6" rot="SR0">&gt;Value</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="LM2576T-ADJ/LF03" prefix="U">
<gates>
<gate name="A" symbol="LM2576T-5_0_1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="T05A_TEX">
<connects>
<connect gate="A" pin="FB" pad="4"/>
<connect gate="A" pin="GND" pad="3"/>
<connect gate="A" pin="OUT" pad="2"/>
<connect gate="A" pin="VIN" pad="1"/>
<connect gate="A" pin="~ON~/OFF" pad="5"/>
</connects>
<technologies>
<technology name="">
<attribute name="COPYRIGHT" value="Copyright (C) 2021 Ultra Librarian. All rights reserved." constant="no"/>
<attribute name="DIGI-KEY_PART_NUMBER_1" value="296-35127-5-ND" constant="no"/>
<attribute name="DIGI-KEY_PART_NUMBER_2" value="2156-LM2576T-ADJ/LF03-ND" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="LM2576T-ADJ/LF03" constant="no"/>
<attribute name="MFR_NAME" value="Texas Instruments" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="ngspice-simulation" urn="urn:adsk.eagle:library:527439">
<description>SPICE compatible library parts</description>
<packages>
</packages>
<symbols>
<symbol name="0" urn="urn:adsk.eagle:symbol:527455/1" library_version="18">
<description>Simulation ground symbol (spice node 0)</description>
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="-2.54" y2="0" width="0.1524" layer="94"/>
<pin name="0" x="0" y="0" visible="off" length="point" direction="sup"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" urn="urn:adsk.eagle:component:527478/1" prefix="X_" library_version="18">
<description>Simulation ground symbol (spice node 0)</description>
<gates>
<gate name="G$1" symbol="0" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name="">
<attribute name="SPICEGROUND" value=""/>
<attribute name="_EXTERNAL_" value=""/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="i HATE ROBOTICS">
<packages>
<package name="95SQ015">
<pad name="1" x="-6.35" y="0" drill="0.889" diameter="1.397"/>
<pad name="2" x="7.62" y="0" drill="0.889" diameter="1.397"/>
<wire x1="2.54" y1="0" x2="7.62" y2="0" width="0.127" layer="21"/>
<wire x1="-1.27" y1="0" x2="-6.35" y2="0" width="0.127" layer="21"/>
<wire x1="0.635" y1="0.762" x2="0.635" y2="-0.762" width="0.127" layer="21"/>
<wire x1="0.635" y1="-0.762" x2="1.397" y2="0" width="0.127" layer="21"/>
<wire x1="1.397" y1="0" x2="0.635" y2="0.762" width="0.127" layer="21"/>
<wire x1="1.397" y1="0" x2="1.397" y2="0.762" width="0.127" layer="21"/>
<wire x1="1.397" y1="0.762" x2="1.143" y2="0.762" width="0.127" layer="21"/>
<wire x1="1.143" y1="0.762" x2="1.143" y2="0.508" width="0.127" layer="21"/>
<wire x1="1.397" y1="0" x2="1.397" y2="-0.762" width="0.127" layer="21"/>
<wire x1="1.397" y1="-0.762" x2="1.651" y2="-0.762" width="0.127" layer="21"/>
<wire x1="1.651" y1="-0.762" x2="1.651" y2="-0.508" width="0.127" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="2.54" y2="1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="-1.27" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="1.27" width="0.127" layer="21"/>
</package>
<package name="ESC108M016AH4AA">
<pad name="1" x="3.81" y="0" drill="0.889" diameter="1.397"/>
<pad name="2" x="-3.81" y="0" drill="0.889" diameter="1.397"/>
<circle x="0" y="0" radius="6.35" width="0.127" layer="21"/>
<wire x1="3.81" y1="3.81" x2="3.81" y2="-3.81" width="0.127" layer="21" curve="180"/>
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="-3.81" width="0.127" layer="21"/>
<wire x1="-3.81" y1="0" x2="3.81" y2="0" width="0.127" layer="21"/>
</package>
<package name="NF123G-302">
<pad name="P$1" x="0" y="7.62" drill="0.6" shape="square"/>
<pad name="P$2" x="0" y="-7.62" drill="0.6" shape="square"/>
<circle x="0" y="0" radius="2.54" width="0.127" layer="21"/>
<wire x1="-2.54" y1="5.08" x2="0" y2="5.08" width="0.127" layer="21"/>
<wire x1="0" y1="5.08" x2="2.54" y2="5.08" width="0.127" layer="21"/>
<wire x1="2.54" y1="5.08" x2="2.54" y2="-5.08" width="0.127" layer="21"/>
<wire x1="2.54" y1="-5.08" x2="0" y2="-5.08" width="0.127" layer="21"/>
<wire x1="0" y1="-5.08" x2="-2.54" y2="-5.08" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-5.08" x2="-2.54" y2="5.08" width="0.127" layer="21"/>
<text x="-0.508" y="3.175" size="1.27" layer="21">+</text>
<text x="-0.254" y="-4.191" size="1.27" layer="21">-</text>
<text x="-0.762" y="-0.254" size="1.27" layer="21">M</text>
<wire x1="0" y1="-7.62" x2="0" y2="-5.08" width="0.127" layer="21"/>
<wire x1="0" y1="5.08" x2="0" y2="7.62" width="0.127" layer="21"/>
</package>
<package name="UHIDKWINIG">
<pad name="P$1" x="-0.08" y="-3.81" drill="1" shape="square"/>
<pad name="P$2" x="0" y="0" drill="1" shape="square"/>
<wire x1="0" y1="1.27" x2="0" y2="1.905" width="0.127" layer="21"/>
<wire x1="0" y1="2.54" x2="0" y2="1.905" width="0.127" layer="21"/>
<wire x1="0" y1="1.905" x2="-0.762" y2="1.905" width="0.127" layer="21"/>
<wire x1="0" y1="1.905" x2="1.016" y2="1.905" width="0.127" layer="21"/>
<wire x1="-0.762" y1="-2.032" x2="0.381" y2="-2.032" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="95SQ015">
<wire x1="0" y1="3.08" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-3.08" width="0.254" layer="94"/>
<wire x1="0" y1="-3.08" x2="7.62" y2="0" width="0.254" layer="94"/>
<wire x1="7.62" y1="0" x2="0" y2="3.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="0" x2="7.62" y2="5.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="5.62" x2="5.08" y2="5.62" width="0.254" layer="94"/>
<wire x1="5.08" y1="5.62" x2="5.08" y2="4.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="0" x2="7.62" y2="-5.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="-5.62" x2="10.16" y2="-5.62" width="0.254" layer="94"/>
<wire x1="10.16" y1="-5.62" x2="10.16" y2="-5.08" width="0.254" layer="94"/>
<pin name="P$1" x="33.7" y="0" length="middle" rot="R180"/>
<pin name="P$2" x="-25.16" y="0" length="middle"/>
<wire x1="0" y1="0" x2="-20" y2="0" width="0.254" layer="94"/>
<wire x1="7" y1="0" x2="33" y2="0" width="0.254" layer="94"/>
</symbol>
<symbol name="ESC108M016AH4AA">
<wire x1="0" y1="5" x2="0" y2="-5" width="0.254" layer="94"/>
<pin name="P$1" x="-5" y="0" length="middle"/>
<wire x1="7" y1="5" x2="7" y2="-5" width="0.254" layer="94" curve="180"/>
<pin name="P$2" x="12" y="0" length="middle" rot="R180"/>
</symbol>
<symbol name="NF123G-302">
<circle x="0" y="0" radius="3.5921" width="0.254" layer="94"/>
<text x="-1.016" y="-0.762" size="1.778" layer="94">M</text>
<wire x1="2.54" y1="2.54" x2="5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="-2.54" x2="2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-2.54" x2="-5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="-5.08" y1="2.54" x2="-2.54" y2="2.54" width="0.254" layer="94"/>
<text x="3.556" y="-0.762" size="1.778" layer="94">+</text>
<text x="-4.826" y="-0.508" size="1.778" layer="94">-</text>
<pin name="P$1" x="10.16" y="0" length="middle" rot="R180"/>
<pin name="P$2" x="-10.16" y="0" length="middle"/>
</symbol>
<symbol name="UHIDKVINIG">
<pin name="+12V" x="-5.08" y="0" length="middle"/>
<pin name="GND" x="-5.08" y="-2.54" length="middle"/>
<text x="-5.08" y="2.54" size="1.27" layer="94">+</text>
<text x="-5.08" y="-5.08" size="1.27" layer="94">-</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="95SQ015">
<gates>
<gate name="G$1" symbol="95SQ015" x="0" y="0"/>
</gates>
<devices>
<device name="" package="95SQ015">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ESC108M016AH4AA">
<gates>
<gate name="G$1" symbol="ESC108M016AH4AA" x="0" y="0"/>
</gates>
<devices>
<device name="" package="ESC108M016AH4AA">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="NF123G-302">
<gates>
<gate name="G$1" symbol="NF123G-302" x="0" y="0"/>
</gates>
<devices>
<device name="" package="NF123G-302">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MOMMY">
<gates>
<gate name="G$1" symbol="UHIDKVINIG" x="0" y="0"/>
</gates>
<devices>
<device name="" package="UHIDKWINIG">
<connects>
<connect gate="G$1" pin="+12V" pad="P$2"/>
<connect gate="G$1" pin="GND" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="IDK3">
<packages>
<package name="CAP_UES_8X11P5_NCH">
<pad name="1" x="0" y="0" drill="0.8636" diameter="1.3716"/>
<pad name="2" x="3.5052" y="0" drill="0.8636" diameter="1.3716" rot="R180"/>
<wire x1="-3.7846" y1="0" x2="-2.5146" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.1496" y1="0.635" x2="-3.1496" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="5.8674" y1="0" x2="-2.3876" y2="0" width="0.1524" layer="21" curve="-180"/>
<wire x1="-2.3876" y1="0" x2="5.8674" y2="0" width="0.1524" layer="21" curve="-180"/>
<wire x1="-3.7846" y1="0" x2="-2.5146" y2="0" width="0.1524" layer="51"/>
<wire x1="-3.1496" y1="0.635" x2="-3.1496" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="5.7404" y1="0" x2="-2.2606" y2="0" width="0" layer="51" curve="-180"/>
<wire x1="-2.2606" y1="0" x2="5.7404" y2="0" width="0" layer="51" curve="-180"/>
<text x="-1.524" y="-0.635" size="1.27" layer="25" ratio="6" rot="SR0">&gt;Name</text>
<text x="0.0254" y="-0.635" size="1.27" layer="27" ratio="6" rot="SR0">&gt;Value</text>
</package>
</packages>
<symbols>
<symbol name="CAPH">
<pin name="2" x="7.62" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="0" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<wire x1="3.4798" y1="-1.905" x2="3.4798" y2="0" width="0.2032" layer="94"/>
<wire x1="3.4798" y1="0" x2="3.4798" y2="1.905" width="0.2032" layer="94"/>
<wire x1="4.1148" y1="-1.905" x2="4.1148" y2="0" width="0.2032" layer="94"/>
<wire x1="4.1148" y1="0" x2="4.1148" y2="1.905" width="0.2032" layer="94"/>
<wire x1="4.1148" y1="0" x2="5.08" y2="0" width="0.2032" layer="94"/>
<wire x1="2.54" y1="0" x2="3.4798" y2="0" width="0.2032" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0.635" width="0.1524" layer="94"/>
<wire x1="0.9652" y1="0.9652" x2="1.5748" y2="0.9652" width="0.1524" layer="94"/>
<text x="-5.1562" y="-5.5372" size="3.4798" layer="96" ratio="10" rot="SR0">&gt;Value</text>
<text x="-4.0894" y="2.0828" size="3.4798" layer="95" ratio="10" rot="SR0">&gt;Name</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="UKL1E101KPDANATD" prefix="C">
<gates>
<gate name="A" symbol="CAPH" x="0" y="0" swaplevel="1"/>
</gates>
<devices>
<device name="" package="CAP_UES_8X11P5_NCH">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="COPYRIGHT" value="Copyright (C) 2021 Ultra Librarian. All rights reserved." constant="no"/>
<attribute name="DIGI-KEY_PART_NUMBER_1" value="493-15360-1-ND" constant="no"/>
<attribute name="DIGI-KEY_PART_NUMBER_2" value="493-15360-3-ND" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="UKL1E101KPDANATD" constant="no"/>
<attribute name="MFR_NAME" value="Nichicon" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="idk4">
<packages>
<package name="IND_BOURNS_78F">
<pad name="1" x="0" y="0" drill="0.762" diameter="1.27" shape="square"/>
<pad name="2" x="11.176" y="0" drill="0.762" diameter="1.27" rot="R180"/>
<wire x1="-2.159" y1="0" x2="-0.9652" y2="0" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="0.635" x2="-1.524" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.524" x2="9.271" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="9.271" y1="-1.524" x2="9.271" y2="1.524" width="0.1524" layer="21"/>
<wire x1="9.271" y1="1.524" x2="1.905" y2="1.524" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.524" x2="1.905" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="0" x2="-0.889" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.524" y1="0.635" x2="-1.524" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="0" y1="0" x2="2.032" y2="0" width="0.1524" layer="51"/>
<wire x1="11.176" y1="0" x2="9.144" y2="0" width="0.1524" layer="51"/>
<wire x1="2.032" y1="-1.397" x2="9.144" y2="-1.397" width="0.1524" layer="51"/>
<wire x1="9.144" y1="-1.397" x2="9.144" y2="1.397" width="0.1524" layer="51"/>
<wire x1="9.144" y1="1.397" x2="2.032" y2="1.397" width="0.1524" layer="51"/>
<wire x1="2.032" y1="1.397" x2="2.032" y2="-1.397" width="0.1524" layer="51"/>
<text x="2.3114" y="-0.635" size="1.27" layer="25" ratio="6" rot="SR0">&gt;Name</text>
<text x="3.8608" y="-0.635" size="1.27" layer="27" ratio="6" rot="SR0">&gt;Value</text>
</package>
</packages>
<symbols>
<symbol name="IND">
<pin name="1" x="15.24" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<pin name="2" x="0" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<wire x1="5.08" y1="0" x2="5.08" y2="1.27" width="0.2032" layer="94"/>
<wire x1="7.62" y1="0" x2="7.62" y2="1.27" width="0.2032" layer="94"/>
<wire x1="12.7" y1="0" x2="12.7" y2="1.27" width="0.2032" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="1.27" width="0.2032" layer="94"/>
<wire x1="10.16" y1="0" x2="10.16" y2="1.27" width="0.2032" layer="94"/>
<wire x1="5.08" y1="1.27" x2="7.62" y2="1.27" width="0.1524" layer="94" curve="-180"/>
<wire x1="2.54" y1="1.27" x2="5.08" y2="1.27" width="0.1524" layer="94" curve="-180"/>
<wire x1="7.62" y1="1.27" x2="10.16" y2="1.27" width="0.1524" layer="94" curve="-180"/>
<wire x1="10.16" y1="1.27" x2="12.7" y2="1.27" width="0.1524" layer="94" curve="-180"/>
<text x="-1.9812" y="-4.2672" size="3.4798" layer="96" ratio="10" rot="SR0">&gt;Value</text>
<text x="-0.9144" y="3.3528" size="3.4798" layer="95" ratio="10" rot="SR0">&gt;Name</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="78F680J-RC" prefix="L">
<gates>
<gate name="A" symbol="IND" x="0" y="0" swaplevel="1"/>
</gates>
<devices>
<device name="" package="IND_BOURNS_78F">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="COPYRIGHT" value="Copyright (C) 2021 Ultra Librarian. All rights reserved." constant="no"/>
<attribute name="DIGI-KEY_PART_NUMBER_1" value="M10144-ND" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="78F680J-RC" constant="no"/>
<attribute name="MFR_NAME" value="Bourns Electronics" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="other stuff idk what to name">
<packages>
<package name="STA_RNMF14_STP">
<pad name="1" x="0" y="0" drill="0.7366" diameter="1.2446" shape="square"/>
<pad name="2" x="7.62" y="0" drill="0.7366" diameter="1.2446" rot="R180"/>
<wire x1="1.8796" y1="-1.0668" x2="5.7404" y2="-1.0668" width="0.1524" layer="21"/>
<wire x1="5.7404" y1="-1.0668" x2="5.7404" y2="1.0668" width="0.1524" layer="21"/>
<wire x1="5.7404" y1="1.0668" x2="1.8796" y2="1.0668" width="0.1524" layer="21"/>
<wire x1="1.8796" y1="1.0668" x2="1.8796" y2="-1.0668" width="0.1524" layer="21"/>
<wire x1="0" y1="0" x2="2.0066" y2="0" width="0.1524" layer="51"/>
<wire x1="7.62" y1="0" x2="5.6134" y2="0" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="-0.9144" x2="5.6134" y2="-0.9144" width="0.1524" layer="51"/>
<wire x1="5.6134" y1="-0.9144" x2="5.6134" y2="0.9144" width="0.1524" layer="51"/>
<wire x1="5.6134" y1="0.9144" x2="2.0066" y2="0.9144" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="0.9144" x2="2.0066" y2="-0.9144" width="0.1524" layer="51"/>
<text x="0.5334" y="-0.635" size="1.27" layer="25" ratio="6" rot="SR0">&gt;Name</text>
<text x="2.0828" y="-0.635" size="1.27" layer="27" ratio="6" rot="SR0">&gt;Value</text>
</package>
</packages>
<symbols>
<symbol name="RES">
<pin name="1" x="0" y="0" visible="pin" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="12.7" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<wire x1="3.175" y1="1.27" x2="4.445" y2="-1.27" width="0.2032" layer="94"/>
<wire x1="4.445" y1="-1.27" x2="5.715" y2="1.27" width="0.2032" layer="94"/>
<wire x1="5.715" y1="1.27" x2="6.985" y2="-1.27" width="0.2032" layer="94"/>
<wire x1="6.985" y1="-1.27" x2="8.255" y2="1.27" width="0.2032" layer="94"/>
<wire x1="8.255" y1="1.27" x2="9.525" y2="-1.27" width="0.2032" layer="94"/>
<wire x1="2.54" y1="0" x2="3.175" y2="1.27" width="0.2032" layer="94"/>
<wire x1="9.525" y1="-1.27" x2="10.16" y2="0" width="0.2032" layer="94"/>
<text x="-2.6162" y="-5.5372" size="3.4798" layer="96" ratio="10" rot="SR0">&gt;Value</text>
<text x="-2.1844" y="2.0828" size="3.4798" layer="95" ratio="10" rot="SR0">&gt;Name</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="RNMF14FTC6K20" prefix="R">
<gates>
<gate name="A" symbol="RES" x="0" y="0" swaplevel="1"/>
</gates>
<devices>
<device name="" package="STA_RNMF14_STP">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="COPYRIGHT" value="Copyright (C) 2021 Ultra Librarian. All rights reserved." constant="no"/>
<attribute name="DIGI-KEY_PART_NUMBER_1" value="S6.2KCACT-ND" constant="no"/>
<attribute name="DIGI-KEY_PART_NUMBER_2" value="S6.2KCATR-ND" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="RNMF14FTC6K20" constant="no"/>
<attribute name="MFR_NAME" value="Stackpole International" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="untitled">
<packages>
<package name="RES_CFR16_TYCO_TYC">
<pad name="1" x="0" y="0" drill="0.889" diameter="1.397" shape="square"/>
<pad name="2" x="7.493" y="0" drill="0.889" diameter="1.397" rot="R180"/>
<wire x1="1.8796" y1="-1.0668" x2="5.6388" y2="-1.0668" width="0.1524" layer="21"/>
<wire x1="5.6388" y1="-1.0668" x2="5.6388" y2="1.0668" width="0.1524" layer="21"/>
<wire x1="5.6388" y1="1.0668" x2="1.8796" y2="1.0668" width="0.1524" layer="21"/>
<wire x1="1.8796" y1="1.0668" x2="1.8796" y2="-1.0668" width="0.1524" layer="21"/>
<wire x1="0" y1="0" x2="2.0066" y2="0" width="0.1524" layer="51"/>
<wire x1="7.493" y1="0" x2="5.5118" y2="0" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="-0.9144" x2="5.5118" y2="-0.9144" width="0.1524" layer="51"/>
<wire x1="5.5118" y1="-0.9144" x2="5.5118" y2="0.9144" width="0.1524" layer="51"/>
<wire x1="5.5118" y1="0.9144" x2="2.0066" y2="0.9144" width="0.1524" layer="51"/>
<wire x1="2.0066" y1="0.9144" x2="2.0066" y2="-0.9144" width="0.1524" layer="51"/>
<text x="0.4826" y="-0.635" size="1.27" layer="25" ratio="6" rot="SR0">&gt;Name</text>
<text x="2.032" y="-0.635" size="1.27" layer="27" ratio="6" rot="SR0">&gt;Value</text>
</package>
</packages>
<symbols>
<symbol name="RES">
<pin name="2" x="0" y="0" visible="pin" length="short" direction="pas" swaplevel="1"/>
<pin name="1" x="12.7" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<wire x1="3.175" y1="1.27" x2="4.445" y2="-1.27" width="0.2032" layer="94"/>
<wire x1="4.445" y1="-1.27" x2="5.715" y2="1.27" width="0.2032" layer="94"/>
<wire x1="5.715" y1="1.27" x2="6.985" y2="-1.27" width="0.2032" layer="94"/>
<wire x1="6.985" y1="-1.27" x2="8.255" y2="1.27" width="0.2032" layer="94"/>
<wire x1="8.255" y1="1.27" x2="9.525" y2="-1.27" width="0.2032" layer="94"/>
<wire x1="2.54" y1="0" x2="3.175" y2="1.27" width="0.2032" layer="94"/>
<wire x1="9.525" y1="-1.27" x2="10.16" y2="0" width="0.2032" layer="94"/>
<text x="-2.6162" y="-5.5372" size="3.4798" layer="96" ratio="10" rot="SR0">&gt;Value</text>
<text x="-2.1844" y="2.0828" size="3.4798" layer="95" ratio="10" rot="SR0">&gt;Name</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="CFR16J1K0" prefix="R">
<gates>
<gate name="A" symbol="RES" x="0" y="0" swaplevel="1"/>
</gates>
<devices>
<device name="" package="RES_CFR16_TYCO_TYC">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="COPYRIGHT" value="Copyright (C) 2021 Ultra Librarian. All rights reserved." constant="no"/>
<attribute name="DIGI-KEY_PART_NUMBER_1" value="A104669CT-ND" constant="no"/>
<attribute name="DIGI-KEY_PART_NUMBER_2" value="A104669TB-ND" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="CFR16J1K0" constant="no"/>
<attribute name="MFR_NAME" value="TE Connectivity" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="frames" urn="urn:adsk.eagle:library:229">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="A3L-LOC" urn="urn:adsk.eagle:symbol:13881/1" library_version="1">
<wire x1="288.29" y1="3.81" x2="342.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="342.265" y1="3.81" x2="373.38" y2="3.81" width="0.1016" layer="94"/>
<wire x1="373.38" y1="3.81" x2="383.54" y2="3.81" width="0.1016" layer="94"/>
<wire x1="383.54" y1="3.81" x2="383.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="383.54" y1="8.89" x2="383.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="383.54" y1="13.97" x2="383.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="383.54" y1="19.05" x2="383.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="288.29" y1="3.81" x2="288.29" y2="24.13" width="0.1016" layer="94"/>
<wire x1="288.29" y1="24.13" x2="342.265" y2="24.13" width="0.1016" layer="94"/>
<wire x1="342.265" y1="24.13" x2="383.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="373.38" y1="3.81" x2="373.38" y2="8.89" width="0.1016" layer="94"/>
<wire x1="373.38" y1="8.89" x2="383.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="373.38" y1="8.89" x2="342.265" y2="8.89" width="0.1016" layer="94"/>
<wire x1="342.265" y1="8.89" x2="342.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="342.265" y1="8.89" x2="342.265" y2="13.97" width="0.1016" layer="94"/>
<wire x1="342.265" y1="13.97" x2="383.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="342.265" y1="13.97" x2="342.265" y2="19.05" width="0.1016" layer="94"/>
<wire x1="342.265" y1="19.05" x2="383.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="342.265" y1="19.05" x2="342.265" y2="24.13" width="0.1016" layer="94"/>
<text x="344.17" y="15.24" size="2.54" layer="94">&gt;DRAWING_NAME</text>
<text x="344.17" y="10.16" size="2.286" layer="94">&gt;LAST_DATE_TIME</text>
<text x="357.505" y="5.08" size="2.54" layer="94">&gt;SHEET</text>
<text x="343.916" y="4.953" size="2.54" layer="94">Sheet:</text>
<frame x1="0" y1="0" x2="387.35" y2="260.35" columns="8" rows="5" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="A3L-LOC" urn="urn:adsk.eagle:component:13942/1" prefix="FRAME" uservalue="yes" library_version="1">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A3, landscape with location and doc. field</description>
<gates>
<gate name="G$1" symbol="A3L-LOC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U1" library="Lab5" deviceset="LM2576T-ADJ/LF03" device=""/>
<part name="X_1" library="ngspice-simulation" library_urn="urn:adsk.eagle:library:527439" deviceset="GND" device=""/>
<part name="X_2" library="ngspice-simulation" library_urn="urn:adsk.eagle:library:527439" deviceset="GND" device=""/>
<part name="X_3" library="ngspice-simulation" library_urn="urn:adsk.eagle:library:527439" deviceset="GND" device=""/>
<part name="X_4" library="ngspice-simulation" library_urn="urn:adsk.eagle:library:527439" deviceset="GND" device=""/>
<part name="X_5" library="ngspice-simulation" library_urn="urn:adsk.eagle:library:527439" deviceset="GND" device=""/>
<part name="X_6" library="ngspice-simulation" library_urn="urn:adsk.eagle:library:527439" deviceset="GND" device=""/>
<part name="U$1" library="i HATE ROBOTICS" deviceset="95SQ015" device="" value="15V"/>
<part name="CIN" library="i HATE ROBOTICS" deviceset="ESC108M016AH4AA" device="" value="100 uf 14v"/>
<part name="U$3" library="i HATE ROBOTICS" deviceset="NF123G-302" device="" value="9 Vout"/>
<part name="COUT" library="IDK3" deviceset="UKL1E101KPDANATD" device="" value="260uf 14v"/>
<part name="L1" library="idk4" deviceset="78F680J-RC" device="" value="68 UH"/>
<part name="R2" library="other stuff idk what to name" deviceset="RNMF14FTC6K20" device="" value="6.3k ohm"/>
<part name="R1" library="untitled" deviceset="CFR16J1K0" device="" value="1k Ohm"/>
<part name="FRAME1" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="A3L-LOC" device=""/>
<part name="U$2" library="i HATE ROBOTICS" deviceset="MOMMY" device=""/>
<part name="X_7" library="ngspice-simulation" library_urn="urn:adsk.eagle:library:527439" deviceset="GND" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="U1" gate="A" x="-5.08" y="20.32" smashed="yes">
<attribute name="NAME" x="23.2156" y="29.4386" size="2.0828" layer="95" ratio="6" rot="SR0"/>
<attribute name="VALUE" x="22.5806" y="26.8986" size="2.0828" layer="96" ratio="6" rot="SR0"/>
</instance>
<instance part="X_1" gate="G$1" x="-12.7" y="-10.16" smashed="yes"/>
<instance part="X_2" gate="G$1" x="58.42" y="10.16" smashed="yes"/>
<instance part="X_3" gate="G$1" x="71.12" y="2.54" smashed="yes"/>
<instance part="X_4" gate="G$1" x="-2.54" y="-66.04" smashed="yes"/>
<instance part="X_5" gate="G$1" x="22.86" y="-15.24" smashed="yes"/>
<instance part="X_6" gate="G$1" x="76.2" y="-22.86" smashed="yes"/>
<instance part="U$1" gate="G$1" x="-2.54" y="-40.64" smashed="yes" rot="R90"/>
<instance part="CIN" gate="G$1" x="-12.7" y="-2.54" smashed="yes" rot="R90"/>
<instance part="U$3" gate="G$1" x="45.72" y="-12.7" smashed="yes" rot="R180"/>
<instance part="COUT" gate="A" x="22.86" y="-2.54" smashed="yes" rot="R270">
<attribute name="VALUE" x="14.7828" y="-7.5438" size="3.4798" layer="96" ratio="10" rot="SR270"/>
<attribute name="NAME" x="24.9428" y="-6.0706" size="3.4798" layer="95" ratio="10" rot="SR270"/>
</instance>
<instance part="L1" gate="A" x="-2.54" y="-2.54" smashed="yes">
<attribute name="VALUE" x="3.0988" y="-4.2672" size="0.8128" layer="96" ratio="10" rot="SR0"/>
<attribute name="NAME" x="1.6256" y="0.8128" size="3.4798" layer="95" ratio="10" rot="SR0"/>
</instance>
<instance part="R2" gate="A" x="45.72" y="-2.54" smashed="yes">
<attribute name="VALUE" x="48.1838" y="-5.5372" size="0.8128" layer="96" ratio="10" rot="SR0"/>
<attribute name="NAME" x="51.1556" y="-0.4572" size="0.8128" layer="95" ratio="10" rot="SR0"/>
</instance>
<instance part="R1" gate="A" x="33.02" y="-2.54" smashed="yes">
<attribute name="VALUE" x="35.4838" y="-5.5372" size="1.016" layer="96" ratio="10" rot="SR0"/>
<attribute name="NAME" x="38.4556" y="-0.4572" size="0.8128" layer="95" ratio="10" rot="SR0"/>
</instance>
<instance part="FRAME1" gate="G$1" x="-142.24" y="-73.66" smashed="yes">
<attribute name="DRAWING_NAME" x="201.93" y="-58.42" size="2.54" layer="94"/>
<attribute name="LAST_DATE_TIME" x="201.93" y="-63.5" size="2.286" layer="94"/>
<attribute name="SHEET" x="215.265" y="-68.58" size="2.54" layer="94"/>
</instance>
<instance part="U$2" gate="G$1" x="-20.32" y="20.32" smashed="yes" rot="R180"/>
<instance part="X_7" gate="G$1" x="-15.24" y="22.86" smashed="yes" rot="R90"/>
</instances>
<busses>
</busses>
<nets>
<net name="0" class="0">
<segment>
<pinref part="U1" gate="A" pin="GND"/>
<pinref part="X_2" gate="G$1" pin="0"/>
<wire x1="58.42" y1="15.24" x2="58.42" y2="10.16" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U1" gate="A" pin="~ON~/OFF"/>
<pinref part="X_3" gate="G$1" pin="0"/>
<wire x1="58.42" y1="20.32" x2="71.12" y2="20.32" width="0.1524" layer="91"/>
<wire x1="71.12" y1="20.32" x2="71.12" y2="2.54" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X_5" gate="G$1" pin="0"/>
<wire x1="22.86" y1="-10.16" x2="22.86" y2="-15.24" width="0.1524" layer="91"/>
<pinref part="COUT" gate="A" pin="2"/>
</segment>
<segment>
<pinref part="X_6" gate="G$1" pin="0"/>
<wire x1="76.2" y1="-2.54" x2="76.2" y2="-22.86" width="0.1524" layer="91"/>
<pinref part="U$3" gate="G$1" pin="P$2"/>
<pinref part="R2" gate="A" pin="2"/>
<wire x1="55.88" y1="-12.7" x2="58.42" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="58.42" y1="-12.7" x2="58.42" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="58.42" y1="-2.54" x2="76.2" y2="-2.54" width="0.1524" layer="91"/>
<junction x="58.42" y="-2.54"/>
</segment>
<segment>
<pinref part="CIN" gate="G$1" pin="P$1"/>
<pinref part="X_1" gate="G$1" pin="0"/>
<wire x1="-12.7" y1="-7.54" x2="-12.7" y2="-10.16" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X_4" gate="G$1" pin="0"/>
<pinref part="U$1" gate="G$1" pin="P$2"/>
<wire x1="-2.54" y1="-66.04" x2="-2.54" y2="-65.8" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="GND"/>
<pinref part="X_7" gate="G$1" pin="0"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="U1" gate="A" pin="OUT"/>
<wire x1="-2.54" y1="17.78" x2="-2.54" y2="-2.54" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="P$1"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="-6.94" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="-6.94" x2="-2.54" y2="-7.62" width="0.1524" layer="91"/>
<junction x="-2.54" y="-6.94"/>
<pinref part="L1" gate="A" pin="2"/>
<junction x="-2.54" y="-2.54"/>
<junction x="-2.54" y="17.78"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<wire x1="33.02" y1="-2.54" x2="22.86" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="22.86" y1="-2.54" x2="12.7" y2="-2.54" width="0.1524" layer="91"/>
<pinref part="COUT" gate="A" pin="1"/>
<junction x="22.86" y="-2.54"/>
<pinref part="L1" gate="A" pin="1"/>
<pinref part="R1" gate="A" pin="2"/>
<pinref part="U$3" gate="G$1" pin="P$1"/>
<wire x1="35.56" y1="-12.7" x2="33.02" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="33.02" y1="-2.54" x2="33.02" y2="-12.7" width="0.1524" layer="91"/>
<junction x="33.02" y="-2.54"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="R1" gate="A" pin="1"/>
<pinref part="R2" gate="A" pin="1"/>
<pinref part="U1" gate="A" pin="FB"/>
<wire x1="58.42" y1="17.78" x2="66.04" y2="17.78" width="0.1524" layer="91"/>
<wire x1="66.04" y1="17.78" x2="66.04" y2="2.54" width="0.1524" layer="91"/>
<wire x1="66.04" y1="2.54" x2="45.72" y2="2.54" width="0.1524" layer="91"/>
<wire x1="45.72" y1="-2.54" x2="45.72" y2="2.54" width="0.1524" layer="91"/>
<junction x="45.72" y="-2.54"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="U1" gate="A" pin="VIN"/>
<wire x1="-12.7" y1="20.32" x2="-2.54" y2="20.32" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="20.32" x2="-12.7" y2="9.46" width="0.1524" layer="91"/>
<pinref part="CIN" gate="G$1" pin="P$2"/>
<wire x1="-12.7" y1="9.46" x2="-12.7" y2="7.62" width="0.1524" layer="91"/>
<junction x="-12.7" y="9.46"/>
<pinref part="U$2" gate="G$1" pin="+12V"/>
<wire x1="-15.24" y1="20.32" x2="-12.7" y2="20.32" width="0.1524" layer="91"/>
<junction x="-12.7" y="20.32"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
<errors>
<approved hash="101,1,-2.54,-65.8,U$1,P$2,,,,"/>
<approved hash="101,1,-12.7,-7.54,CIN,P$1,,,,"/>
<approved hash="104,1,-2.54,17.78,U1,OUT,N$2,,,"/>
<approved hash="104,1,58.42,15.24,U1,GND,0,,,"/>
<approved hash="113,1,51.331,56.411,FRAME1,,,,,"/>
</errors>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
